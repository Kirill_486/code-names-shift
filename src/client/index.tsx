import * as React from "react";
import * as ReactDom from "react-dom";

const entryPointJsx = (
    <div className="app-root__container">
        <span className="app-root__message">The codenames shift is up and running</span>
    </div>
);

ReactDom.render(entryPointJsx, document.getElementById("app"));
