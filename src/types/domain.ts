export interface IJob {
    jobId: string;
    isCommandJob: boolean;
}

export interface IPerson {
    codeName: string;
    qualification: IJob[];    
}

export interface IDuty {
    work: IJob;
    number: number;
    personel: IPerson[];

    isFullfilled?: () => boolean;
}

export interface IDirection {
    locationCode: string;
    consist: IDuty[];

    isFullfilled?: () => boolean;
}

export interface IShift {
    shiftCode: string;
    directions: IDirection[];

    isFullfilled?: ()  => boolean;
}

export interface IDay {
    date: number;
    order: IShift[];

    isFullfilled?: () => boolean;
}
