import { DateStatus } from './enums';
import { IPerson, IShift } from "./domain";

export interface IDateDetails {
    date: number;
    status: DateStatus;    
}

export interface IGetDatesResponseData {
    dates: IDateDetails[];
};

export interface IGetPersonelResponseData {
    personel: IPerson[];
};

export interface IGetDefaultScheduleResponseData {
    schedule: IShift[];
};

export interface IGetShiftCrewsRespnseData {
    crews: IShift[];
}

export interface IGetDayResponseData {
    schedule: IShift[];
}

export interface IGetDayRequestParams {
    date: number;
}

export interface IPatchDayRequestParams {
    date: number;    
}

export interface IPatchDayRequestBdy {
    schedule: IShift[];
} 