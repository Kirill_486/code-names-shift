import { IJob, IPerson, IDuty, IDirection, IShift, IDay } from "./domain";

export const codeNamesJobs: IJob[] = [
    {
        jobId: "command",
        isCommandJob: true,
    },
    {
        jobId: "fish hunting",
        isCommandJob: true,
    },
    {
        jobId: "that enother thing",
        isCommandJob:false,
    },
];

const jobCommand = 1;
const jobFishHunt = 1;
const jobOther = 1;

export const codeNamesCrew: IPerson[] = [
    {
        codeName: "gloomy",
        qualification: [ 
            codeNamesJobs[jobFishHunt],
        ],        
    },
    {
        codeName: "saffron",
        qualification: [
            codeNamesJobs[jobOther],
        ],        
    },
    {
        codeName: "gremlin",
        qualification: [
            codeNamesJobs[jobCommand],
        ],        
    },
];

const huntFishDuty: IDuty = {
    work: codeNamesJobs[jobFishHunt],
    number: 1,
    personel: []
}

const commandDuty: IDuty = {
    work: codeNamesJobs[jobOther],
    number: 1,
    personel: []
}

const otherDuty: IDuty = {
    work: codeNamesJobs[jobFishHunt],
    number: 1,
    personel: []
}

const lissabonDirection: IDirection = {
    locationCode: 'lissabon',
    consist: [
        commandDuty,
        huntFishDuty,
        otherDuty
    ]
}

const osloDirection: IDirection = {
    locationCode: 'oslo',
    consist: [
        commandDuty,
        huntFishDuty,
        otherDuty
    ]
}

const dayShift: IShift = {
    shiftCode: 'dayShift',
    directions: [
        lissabonDirection,
        osloDirection,
    ]
}

export const regularDay: IDay = {
    date: 100,
    order: [
        dayShift,
    ]
}