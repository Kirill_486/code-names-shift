import { getPersonelController, getCrewsController } from './controller/personelController';
import * as express from "express";
import { publicPath } from "./constants";

import { homeController } from "./controller/homeController";
import { getDatesController, getDayController, patchDayController } from "./controller/datesController";
import { getDefaultScheduleController } from './controller/scheduleController';


class App {
  public express;

  constructor() {
    this.express = express();
    this.mountRoutes();
    this.setStaticFilesUrl();
  }

  private mountRoutes(): void {
    const router = express.Router();
    
    router.get("api/dates", getDatesController);
    router.get("api/day/:date", getDayController);
    router.patch("api/day/:date", patchDayController);

    router.get("api/personel", getPersonelController);
    router.get("api/crews", getCrewsController);
    
    router.get("api/schedule", getDefaultScheduleController);

    router.get("/", homeController);
    this.express.use("/", router);
  }

  private setStaticFilesUrl(): void {
    this.express.use(express.static(publicPath));
  }
}

export default new App().express;
