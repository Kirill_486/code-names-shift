import { IGetDefaultScheduleResponseData } from "../../types/api";

export const getDefaultSchedule =
(): IGetDefaultScheduleResponseData => {
    return {
        schedule: []
    }
};
