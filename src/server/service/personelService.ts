import { IGetShiftCrewsRespnseData, IGetPersonelResponseData } from './../../types/api';

export const getPersonel =
(): IGetPersonelResponseData => {
    return {
        personel: []
    };
};

export const getCrews =
(): IGetShiftCrewsRespnseData => {
    return {
        crews: []
    }
}