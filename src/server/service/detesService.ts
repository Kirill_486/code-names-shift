import { IGetDatesResponseData, IGetDayResponseData } from './../../types/api';
import { IShift } from '../../types/domain';

export const getDates =
(): IGetDatesResponseData => {
    return {
        dates: []
    }
}

export const getDay =
(
    date: number
): IGetDayResponseData => {
    return {
        schedule: []
    }
}

export const patchDay =
(
    date: number,
    schedule: IShift[]
) => {
    return true;
}