import * as express from "express";
import { getPersonel, getCrews } from "../service/personelService";

export const getPersonelController =
(
    req: express.Request,
    res: express.Response,
) => {
    const responseData = getPersonel();
    res.json(responseData);
};

export const getCrewsController =
(
    req: express.Request,
    res: express.Response,
) => {
    const responseData = getCrews();
    res.json(responseData);
}