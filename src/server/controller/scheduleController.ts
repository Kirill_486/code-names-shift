import * as express from "express";
import { getDefaultSchedule } from "../service/scheduleService";

export const getDefaultScheduleController =
(
    req: express.Request,
    res: express.Response,
) => {
    const responseData = getDefaultSchedule();
    res.json(responseData);
};