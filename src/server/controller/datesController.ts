import { IGetDayRequestParams, IPatchDayRequestParams } from './../../types/api';
import { getDates, getDay, patchDay } from './../service/detesService';
import * as express from "express";

export const getDatesController = (
    req: express.Request,
    res: express.Response,
  ) => {
    const responseData = getDates();
    res.json(responseData);
};

export const getDayController = (
    req: express.Request,
    res: express.Response,
  ) => {
    const params: IGetDayRequestParams = req.params;
    const responseData = getDay(params.date);
    res.json(responseData);
};

export const patchDayController = (
    req: express.Request,
    res: express.Response,
  ) => {
    const params: IPatchDayRequestParams = req.params;
    const schedule = req.body;
    const responseData = patchDay(params.date, schedule);
    res.json(responseData);
};


