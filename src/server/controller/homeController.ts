import * as express from "express";
import * as path from "path";
import { publicPath } from "../constants";
import { fileName } from "./../constants";

export const homeController =
(
  req: express.Request,
  res: express.Response,
) => {
  res.sendFile(path.join(publicPath, fileName));
};
